import { Component } from '@angular/core';
import {FireBaseApiService} from "./fireBaseApi.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular';

  constructor(private fireBaseApiService: FireBaseApiService) {
    this.fireBaseApiService.initDB();
  }

  get tobuyList() {
    return this.fireBaseApiService.getDBdata();
  }

  add() {
    const testItem = {
      title: 'newItem -' + (+(new Date())),
      amount: Math.round(Math.random() * 100),
      checked: false
    };

    this.fireBaseApiService.add(testItem);
  }

  update(item) {
    item.title = 'updatedItem -' + (+(new Date()))

    this.fireBaseApiService.update(item.id, item);
  }

  remove(item) {
    this.fireBaseApiService.remove(item.id);
  }
}
